<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	
	public function __construct($username, $password)
	{
		return parent::__construct($username, $password);
	}
	
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$username=strtolower($this->username);
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		
		// If not found check email
		if($user===null)
			$user=User::model()->findByAttributes(array('email'=>$this->username));
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!CPasswordHelper::verifyPassword($this->password, $user->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id=$user->id;
			$this->username=$user->username;
			$this->errorCode=self::ERROR_NONE;
			//if($user->role==1 && $user->status==1) Yii::app()->user->setState('isAdmin', 1);
			//if($user->status==1) Yii::app()->user->setState('isActivated', 1);
		}
		return $this->errorCode==self::ERROR_NONE;
	}
	
	public function getId()
	{
		return $this->_id;
	}
	
}