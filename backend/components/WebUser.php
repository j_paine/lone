<?php

class WebUser extends CWebUser {
	private $_model = null;
	
	public function getRole()
	{
		$user=$this->loadUser(Yii::app()->user->id);
		if($user!==null)
			return $user->role;
	}
	
	public function getStatus()
	{
		$user=$this->loadUser(Yii::app()->user->id);
		if($user!==null)
			return $user->status;
	}
	
	protected function loadUser($id)
	{
		if($this->_model===null)
		{
			if($id!==null)
				$this->_model=User::model()->findByPk($id);
		}
		return $this->_model;
	}
}