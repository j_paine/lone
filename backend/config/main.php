<?php
$root=dirname(__FILE__).'/../..';
$params=array_merge(
	require($root.'/common/config/params.php'),
	require($root.'/backend/config/params.php'),
	require($root.'/backend/config/params-local.php')
);

return array(
	'name'=>'Hundeverdenkontrol',
	'basePath'=>$root.'/backend',
	'params'=>$params,
	'preload'=>array('log'),

	// path aliases
	'aliases' => array(
		'bootstrap' => realpath(__DIR__ . '/../../common/extensions/bootstrap'), // change this if necessary
		'yiiwheels' => realpath(__DIR__ . '/../../common/extensions/yiiwheels'),
		'easyimage' => realpath(__DIR__ . '/../../common/extensions/easyimage'),
	),
	'import'=>array(
		'site.common.components.*',
		'site.common.models.*',
		'application.components.*',
		'application.models.*',
		'bootstrap.helpers.TbHtml',
		'bootstrap.helpers.TbArray',
		'bootstrap.behaviors.TbWidget',
		'bootstrap.widgets.*',
		'easyimage.EasyImage',
	),

	// application components
	'components'=>array(
		'easyImage' => array(
			'class' => 'easyimage.EasyImage',
			'cachePath' => '/../../images/cache',
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
		'yiiwheels'=>array(
			'class' => 'yiiwheels.YiiWheels',
		),
		'user'=>array(
			'class'=>'WebUser',
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=doggiedb',
			'emulatePrepare' => true,
			'enableParamLogging' => true,
			'username' => 'mysql',
			'password' => 'donkey',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),
);
