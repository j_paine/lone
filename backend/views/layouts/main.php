<?php /* @var $this Controller */ ?>
<?php Yii::app()->bootstrap->register(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
<?php echo CHtml::image(Yii::app()->params['frontend.url'].'/images/header.jpg'); ?>
	<div id="mainmenu">
		<?php $items=(Yii::app()->user->role==='1' || Yii::app()->user->status==='1')
			? array(
						array('label'=>'Home', 'url'=>array('/site/index')),
						array('label'=>'Dogs', 'url'=>array('/dog/index')),
						array('label'=>'Pics', 'url'=>array('/photo/gallery')),
						array('label'=>'Blog', 'url'=>array('/post/index')),
						array('label'=>'Frontend Site', 'url'=>Yii::app()->params['frontend.url']),
						array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					)
			: array(
						array('label'=>'Frontend Site', 'url'=>Yii::app()->params['frontend.url']),
						array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					);
		$this->widget('bootstrap.widgets.TbNavbar',array(
			'brandLabel'=>CHtml::encode(Yii::app()->name),
			'fluid'=>true,
			'collapse'=>true,
			'items'=>array(
				array(
					'class'=>'bootstrap.widgets.TbNav',
					'items'=>$items,
				),
			),
		)); ?>
	</div><!-- mainmenu -->
	

	
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
