<?php
/* @var $this PhotoController */
/* @var $model Photo */
?>

<?php
$this->breadcrumbs=array(
	'Photos'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Photo', 'url'=>array('index')),
	array('label'=>'View Photo', 'url'=>array('view', 'id'=>$model->id)),
);

if(Yii::app()->user->status==='1')
	$this->menu[]=array('label'=>'Create Photo', 'url'=>array('create'));

if(Yii::app()->user->role==='1')
	$this->menu[]=array('label'=>'Manage Photo', 'url'=>array('admin'));

?>

    <h1>Update Photo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('common.views.photo._form', array('model'=>$model)); ?>