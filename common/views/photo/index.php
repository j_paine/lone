<?php
/* @var $this PhotoController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Photos',
);

$this->menu=array();
if(Yii::app()->user->id!==null)
{
	if(Yii::app()->user->status==='1')
		$this->menu[]=array('label'=>'Add Photos', 'url'=>array('create'));

	if(Yii::app()->user->role==='1')
		$this->menu[]=array('label'=>'Manage Photo', 'url'=>array('admin'));
}
?>

<h1>Photos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'common.views.photo._view',
)); ?>