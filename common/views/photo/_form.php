<?php
/* @var $this PhotoController */
/* @var $model Photo */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php /*$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'photo-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	//'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); */?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php //echo $form->errorSummary($model); ?>

            <?php //echo $form->textFieldControlGroup($model,'title',array('span'=>5,'maxlength'=>255)); ?>

            <?php //echo $form->fileFieldControlGroup($model,'filename'); ?>
	<?php
		$this->widget(
			'yiiwheels.widgets.fileupload.WhFileUpload',
			array('name' => 'fileuploadui',
				'model'=>$model,
				'attribute'=>'filename',
				'url' => $this->createUrl('photo/upload', array('type' => 'fine')),
				'multiple' => true,
			)
		);
    ?>

        <div class="form-actions">
        <?php /*echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); */?>
    </div>

    <?php //$this->endWidget(); ?>

</div><!-- form -->