<?php
/* @var $this PhotoController */
/* @var $model Photo */
?>

<?php
$this->breadcrumbs=array(
	'Photos'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Photo', 'url'=>array('index')),
	array('label'=>'Manage Photo', 'url'=>array('admin')),
);

if(Yii::app()->user->status==='1')
{
	$this->menu[]=array('label'=>'Create Photo', 'url'=>array('create'));
	$this->menu[]=array('label'=>'Update Photo', 'url'=>array('update'));
}


if(Yii::app()->user->role==='1')
{
	$this->menu[]=array('label'=>'Manage Photo', 'url'=>array('admin'));
	$this->menu[]=array('label'=>'Delete Photo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'));
}
?>

<h1>View Photo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'title',
		'filename',
		'type',
		'created',
		'updated',
	),
)); ?>

<?php echo CHtml::image(
		Yii::app()->request->baseUrl.'/photo/img/'.$model->id,
		$model->filename
); ?>
