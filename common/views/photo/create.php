<?php
/* @var $this PhotoController */
/* @var $model Photo */
?>

<?php
$this->breadcrumbs=array(
	'Photos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Photo', 'url'=>array('index')),
);

if(Yii::app()->user->role==='1')
	$this->menu[]=array('label'=>'Manage Photo', 'url'=>array('admin'));
	
?>

<h1>Upload Photos</h1>

<?php $this->widget(
	'yiiwheels.widgets.fileupload.WhFileUpload',
	array('name' => 'photo-form',
		'model'=>$model,
		'attribute'=>'filename',
		'url' => $this->createUrl('photo/upload'), //, array('type' => 'fine')),
		//'htmlOptions'=>array('class'=>'well'),
	)
);?>

<?php //$this->renderPartial('common.views.photo._form', array('model'=>$model)); ?>