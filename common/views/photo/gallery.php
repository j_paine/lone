<?php
/* @var $this PhotoController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Photos',
);

$this->menu=array(
	array('label'=>'Add Photos','url'=>array('create')),
);

if(Yii::app()->user->role==='1')
	$this->menu[]=array('label'=>'Manage Photo', 'url'=>array('admin'));

?>

<h1>Photos</h1>

<?php $data=$dataProvider->getData(); 
$items=[];
foreach($data as $item)
{
	$items[]=array(
		'url'=>Yii::app()->baseUrl.$this->createUrl('img', array('id'=>$item->id)),
		'src'=>Yii::app()->baseUrl.$this->createUrl('thumb', array('id'=>$item->id)),
	);
	
}
?>
<?php $this->widget('yiiwheels.widgets.gallery.WhGallery', array(
	'items' => $items,
	'displayControls'=>true,
));?>

