<?php
/* @var $this PostController */
/* @var $dataProvider CActiveDataProvider */
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['frontend.url']; ?>/css/post.css" />

<?php
$this->breadcrumbs=array(
	'Posts',
);

$this->menu=array();

if(Yii::app()->user->status==='1')
	$this->menu[]=array('label'=>'Create Post','url'=>array('create'));

if(Yii::app()->user->role==='1')
	$this->menu[]=array('label'=>'Manage Post','url'=>array('admin'));
?>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'common.views.post._view',
)); ?>