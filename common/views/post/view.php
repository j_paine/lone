<?php
/* @var $this PostController */
/* @var $model Post */
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['frontend.url']; ?>/css/post.css" />

<?php
Yii::app()->clientScript->registerScript('comment', "
$('.form').toggle();
$('#comment').click(function(){
	$('.form').toggle();
	return false;
});
", CClientScript::POS_END);

$this->breadcrumbs=array(
	'Posts'=>array('index'),
	$model->title,
);
$this->menu=array(
	array('label'=>'List Posts', 'url'=>array('index')),
);

if(Yii::app()->user->status==='1')
{
	$this->menu[]=array('label'=>'Create Post', 'url'=>array('create'));
	$this->menu[]=array('label'=>'Update Post', 'url'=>array('update', 'id'=>$model->id));
} 

if(Yii::app()->user->role==='1')
{
	$this->menu[]=array('label'=>'Delete Post', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'));
	$this->menu[]=array('label'=>'Manage Post', 'url'=>array('admin'));
}
?>

<?php $this->renderPartial('common.views.post._view', array('model'=>$model)); ?>

<h3><a name="comment" id="comment">Leave a comment</a></h3>

<?php $this->renderPartial('common.views.comment._form', array('model'=>$comment,'parent'=>$model)); ?>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$model->comments,
	'itemView'=>'common.views.comment._view',
)); ?>

<?php /*foreach($model->comments as $comment): ?>
	<div class="post-content">
		<?php echo $comment->name; ?> wrote:
		<div class="post-info">at <?php echo $comment->created; ?></div><br /> 
		<div class="post-content">
			<?php echo $comment->content; ?>
		</div>
	</div><!-- post-comment -->
<?php endforeach;*/ ?>	