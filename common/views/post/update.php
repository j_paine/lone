<?php
/* @var $this PostController */
/* @var $model Post */
?>

<?php
$this->breadcrumbs=array(
	'Posts'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);
if(Yii::app()->user->getState('isAdmin'))
{
	$this->menu=array(
		array('label'=>'List Post', 'url'=>array('index')),
		array('label'=>'Create Post', 'url'=>array('create')),
		array('label'=>'View Post', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Post', 'url'=>array('admin')),
	);
} else {
	$this->menu=array(
		array('label'=>'List Post', 'url'=>array('index')),
		array('label'=>'View Post', 'url'=>array('view', 'id'=>$model->id)),
	);
}
?>

    <h1>Update Post <?php echo $model->id; ?></h1>

<?php $this->renderPartial('common.views.post._form', array('model'=>$model)); ?>