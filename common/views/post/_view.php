<?php
/* @var $this PostController */
/* @var $data Post */

if(isset($model))
	$d=$model;
else
{
	$d=$data;
	$d->trimContent();
}

?>

<div class="post">
	
	<div class="post-header">
		<?php echo CHtml::image(Yii::app()->params['frontend.url'].'/images/paw.jpeg'); ?>
		<?php echo CHtml::encode($d->title); ?>
	</div><!-- post-header -->
	<div class="post-info">
			by <?php echo CHtml::encode($d->user->username); ?>
			on <?php echo CHtml::encode($d->created); ?>
		</div>
	<div class="post-content">
		<?php echo $d->content; ?>
	</div><!-- post-content -->
	
	<div class="post-footer">
		<div class="post-info">
			<?php if(isset($d->updated)): ?> 
				<?php echo CHtml::encode(lcfirst($d->getAttributeLabel('updated'))); ?> on
				<?php echo CHtml::encode($d->updated); ?>
			<?php endif; ?>
		</div>
		<?php if(isset($data)): ?>
			<ul>
				<li><?php if($d->clipped): ?>
					<?php echo CHtml::link("Show more",array('view','id'=>$d->id,'#'=>'comment')); ?>
					<?php endif; ?></li>
				<li><?php echo CHtml::link("Comments",array('view','id'=>$d->id)); ?></li>
			</ul>
		<?php endif; ?>
	</div><!-- post-footer -->
	
</div><!-- view -->