<?php
/* @var $this DogController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Dogs',
);

if(Yii::app()->user->getState("isAdmin"))
{
	$this->menu=array(
	array('label'=>'Create Dog','url'=>array('create')),
	array('label'=>'Manage Dog','url'=>array('admin')),
);
}

?>

<h1>Dogs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'common.views.dog._view',
)); ?>