<?php
/* @var $this DogController */
/* @var $model Dog */
?>

<?php
$this->breadcrumbs=array(
	'Dogs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Dog', 'url'=>array('index')),
	array('label'=>'Manage Dog', 'url'=>array('admin')),
);
?>

<h1>Add a new dog</h1>

<?php $this->renderPartial('common.views.dog._form', array('model'=>$model)); ?>