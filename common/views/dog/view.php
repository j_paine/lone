<?php
/* @var $this DogController */
/* @var $model Dog */
?>

<?php
$this->breadcrumbs=array(
	'Dogs'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Dog', 'url'=>array('index')),
	array('label'=>'Create Dog', 'url'=>array('create')),
	array('label'=>'Update Dog', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Dog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dog', 'url'=>array('admin')),
);
?>

<h1>View Dog #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'name',
		'comment',
		'dob',
		'dod',
		'mum_id',
		'dad_id',
		'created',
		'updated',
	),
)); ?>