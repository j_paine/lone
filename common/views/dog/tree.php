<?php
/* @var $this DogController */
/* @var $dogs array */
?>

<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

<?php
$this->breadcrumbs=array(
	'Dogs',
);

if(Yii::app()->user->getState("isAdmin"))
{
	$this->menu=array(
	array('label'=>'Create Dog','url'=>array('create')),
	array('label'=>'Manage Dog','url'=>array('admin')),
);
}
?>

<h1>Dogs</h1>

<style>

	circle {
		stroke-width: 1.5px;
	}

	line {
		stroke: #999;
	}
 
</style>

<div id="content">
	
	
	
</div>

<script>
	var data = <?php echo CJSON::encode($dogs); ?>;
	var i=0;
var dataMap = data.reduce(function(map, node) {
  map[node.id] = i++;
  return map;
}, {});

var nodes = [],
    links = [],
    labels = [];

data.forEach(function(node) {
  nodes.push(node);
  labels.push({"name": node.name});
  if (node.mum_id) {
    links.push({"source":dataMap[node.mum_id], "target":dataMap[node.id]});
  } 
  if (node.dad_id) {
    links.push({"source":dataMap[node.dad_id], "target":dataMap[node.id]});
  }
});

console.log(nodes);
console.log(links);

var w = 500,
    h = 500,
    r = 6,
    fill = d3.scale.category20();

var force = d3.layout.force()
    .charge(-380)
    .linkDistance(100)
    //.gravity(0)
    .size([w, h]);

var svg = d3.select("#content").append("svg:svg")
    .attr("width", w)
    .attr("height", h);

var link = svg.selectAll("line")
    .data(links)
    .enter()
    .append("svg:line");

var label = svg.selectAll("text")
    .data(nodes)
    .enter()
    .append("svg:text")
    .text(function(d) {
      return d.name;
    })
    .attr("x", function(d) { return d.x; })
    .attr("y", function(d) { return d.y; })
    .attr("font-family", "sans-serif")
	.attr("font-size", "11px")
	.attr("fill", "red");
    

var node = svg.selectAll("circle")
    .data(nodes)
    .enter().append("svg:circle")
    .attr("r", r - 0.75)
    .style("fill", function(d) { return d.gender==1?"blue":"pink";})//fill(d.group); })
    .style("stroke", function(d) { 
      return d.gender==1?"light-blue":"magenta";//d3.rgb(fill(d.group)).darker(); 
    })
	.on("click", "info")
    .call(force.drag);

  force
    .nodes(nodes)
    .links(links)
    .on("tick", tick)
    .start();

function info(d) {
	if (d3.event.defaultPrevented) return;
	d.attr("fill", "green");
}

function tick(e) {

    // Push sources up and targets down to form a weak tree.
    var k = 6 * e.alpha;
    links.forEach(function(d, i) {
      d.source.y -= k;
      d.target.y += k;
    });

    node.attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });

    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });
  
    label.attr("x", function(d) { return d.x + 10; })
        .attr("y", function(d) { return d.y; });
}

</script>
	