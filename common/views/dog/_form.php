<?php
/* @var $this DogController */
/* @var $model Dog */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('WhActiveForm', array(
		'id'=>'dog-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>true,
		'enableClientValidation'=>true,
		'helpType'=>TbHtml::HELP_TYPE_INLINE,
		'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
		'htmlOptions'=>array('class'=>'well'),
	)); ?>

    <p class="help-inline">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldControlGroup($model,'name',array('span'=>5,'maxlength'=>255)); ?>

		<?php echo $form->radioButtonListControlGroup($model, 'gender', array(0=>'Female',1=>'Male')); ?>
	
		<?php $bdate = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker',array(
			'model'=>$model,
			'attribute'=>'dob',
			'value' =>$model->dob,
			'pluginOptions'=>array(
				'format'=>'dd-mm-yy',
			),
		), true);?>
		<?php echo $form->customControlGroup($bdate, $model, 'dob'); ?>
		
		<?php $ddate = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker',array(
			'model'=>$model,
			'attribute'=>'dod',
			'value' =>$model->dod,
			'pluginOptions'=>array(
				'format'=>'dd-mm-yy',
			),
		), true);?>
		<?php echo $form->customControlGroup($ddate, $model, 'dod'); ?>
		
	
		<?php $clause=($model->id) ? ' AND id!='.$model->id:''; ?>

		<?php echo $form->dropDownListControlGroup($model,'mum_id',
				CMap::mergeArray(array(null=>''),	
					CHtml::listData(Dog::model()->findAll('gender=0'.$clause), 'id', 'name'))); ?>

		<?php echo $form->dropDownListControlGroup($model,'dad_id',
				CMap::mergeArray(array(null=>''),
					CHtml::listData(Dog::model()->findAll('gender=1'.$clause), 'id', 'name'))); ?>

		<?php echo $form->textFieldControlGroup($model,'stmb',array('span'=>3,'maxlength'=>50)); ?>

		<?php echo $form->textAreaControlGroup($model,'comment',array('rows'=>4,'span'=>5)); ?>

		<?php echo TbHtml::formActions(array(
			TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
				'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
				'size'=>TbHtml::BUTTON_SIZE_DEFAULT,
			)),
			TbHtml::resetButton('Reset',array(
				'color'=>TbHtml::BUTTON_COLOR_DEFAULT,
				'size'=>TbHtml::BUTTON_SIZE_DEFAULT,
			)),
		)); ?>
        
    

    <?php $this->endWidget(); ?>

</div><!-- form -->