<?php
/* @var $this DogController */
/* @var $data Dog */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dob')); ?>:</b>
	<?php echo CHtml::encode($data->dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dod')); ?>:</b>
	<?php echo CHtml::encode($data->dod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mum_id')); ?>:</b>
	<?php echo CHtml::encode($data->mum['name']); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dad_id')); ?>:</b>
	<?php echo CHtml::encode($data->dad['name']); ?>
	<br />

</div>