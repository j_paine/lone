<?php

class m140603_113722_create_user_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('user', array(
			'id'=>'pk',
			'username'=>'string NOT NULL',
			'auth_key'=>'VARCHAR(32)',
			'password'=>'string NOT NULL',
			'password_reset_token'=>'string NULL',
			'email'=>'string NOT NULL',
			'role'=>'SMALLINT(6) NOT NULL DEFAULT 0',
			'status'=>'SMALLINT(6) NOT NULL DEFAULT 0',
			'photo_id'=>'integer NULL',
			'created'=>'datetime NOT NULL',
			'updated'=>'datetime DEFAULT NULL',
		));
		
		$this->createIndex('idx_user_username', 'user', 'username', true);
		$this->createIndex('idx_user_email', 'user', 'email', true);
		
		$this->addForeignKey(
			'fk_user_photo', 
			'user', 'photo_id', 
			'photo', 'id', 
			'SET NULL', 'NO ACTION'
		);
		
		$this->insert('user', array(
			'username'=>'jim',
			'email'=>'jim@mail.com',
			'role'=>1,
			'status'=>1,
			'created'=>new CDbExpression('NOW()'),
		));
		
		$this->insert('user', array(
			'username'=>'lone',
			'email'=>'lone@mail.com',
			'role'=>1,
			'status'=>1,
			'created'=>new CDbExpression('NOW()'),
		));
		
		$this->insert('user', array(
			'username'=>'test',
			'email'=>'test@mail.com',
			'role'=>0,
			'status'=>1,
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('user');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}