<?php

class m140603_113707_create_photo_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('photo', array(
			'id'=>'pk',
			'title'=>'string DEFAULT NULL',
			'path'=>'text NOT NULL',
            'type'=>'SMALLINT(6) DEFAULT 0',
			'created'=>'datetime NOT NULL',
			'updated'=>'datetime DEFAULT NULL',
		));
		
		$this->createIndex('title', 'photo', 'title');
		
	}

	public function down()
	{
		$this->dropTable('photo');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}