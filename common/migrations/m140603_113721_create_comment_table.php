<?php

class m140603_113721_create_comment_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('comment', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'email'=>'string DEFAULT NULL',
			'content'=>'text',
			'visible'=>'boolean DEFAULT 0',
			'created'=>'datetime NOT NULL',
			'updated'=>'datetime DEFAULT NULL',
		));
		
		$this->createIndex('name', 'comment', 'name');
		
		$this->insert('comment', array(
			'name'=>'anonymous',
			'content'=>'Wow great post!',
			'visible'=>1,
			'created'=>new CDbExpression('NOW()'),
		));
		
	}

	public function down()
	{
		$this->dropTable('comment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}