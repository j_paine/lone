<?php

class m140603_211319_create_photo_has_comment_table extends CDbMigration
{
	public function up()
	{	
		$this->createTable('photo_has_comment', array(
			'photo_id'=>'integer NOT NULL',
			'comment_id'=>'integer NOT NULL',
		));
		
		$this->addPrimaryKey('pk_photo_has_comment', 'photo_has_comment', array('photo_id','comment_id'));
		
		$this->addForeignKey(
			'fk_photo_has_comment_photo', 
			'photo_has_comment', 'photo_id', 
			'photo', 'id', 
			'CASCADE', 'NO ACTION'
		);
		$this->addForeignKey(
			'fk_photo_has_comment_comment', 
			'photo_has_comment', 'comment_id', 
			'comment', 'id', 
			'CASCADE', 'NO ACTION'
		);
	}

	public function down()
	{
		$this->dropTable('photo_has_comment');
	}
	
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}