<?php

class m140603_211305_create_dog_has_comment_table extends CDbMigration
{
	public function up()
	{	
		$this->createTable('dog_has_comment', array(
			'dog_id'=>'integer NOT NULL',
			'comment_id'=>'integer NOT NULL',
		));
		
		$this->addPrimaryKey('pk_dog_has_comment', 'dog_has_comment', array('dog_id','comment_id'));
		
		$this->addForeignKey(
			'fk_dog_has_comment_dog', 
			'dog_has_comment', 'dog_id', 
			'dog', 'id', 
			'CASCADE', 'NO ACTION'
		);
		$this->addForeignKey(
			'fk_dog_has_comment_comment', 
			'dog_has_comment', 'comment_id', 
			'comment', 'id', 
			'CASCADE', 'NO ACTION'
		);
	}

	public function down()
	{
		$this->dropTable('dog_has_comment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}