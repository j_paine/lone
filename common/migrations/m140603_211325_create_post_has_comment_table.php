<?php

class m140603_211325_create_post_has_comment_table extends CDbMigration
{
	public function up()
	{	
		$this->createTable('post_has_comment', array(
			'post_id'=>'integer NOT NULL',
			'comment_id'=>'integer NOT NULL',
		));
		
		$this->addPrimaryKey('pk_post_has_comment', 'post_has_comment', array('post_id','comment_id'));
		
		$this->addForeignKey(
			'fk_post_has_comment_post', 
			'post_has_comment', 'post_id', 
			'post', 'id', 
			'CASCADE', 'NO ACTION'
		);
		$this->addForeignKey(
			'fk_post_has_comment_comment', 
			'post_has_comment', 'comment_id', 
			'comment', 'id', 
			'CASCADE', 'NO ACTION'
		);
		
		$this->insert('post_has_comment', array(
			'post_id'=>1,
			'comment_id'=>1,
		));
	}

	public function down()
	{
		$this->dropTable('post_has_comment');
	}
	

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}