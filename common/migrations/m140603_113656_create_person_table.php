<?php

class m140603_113656_create_person_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('person', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'shortname'=>'string NULL',
			'email'=>'string DEFAULT NULL',
			'type'=>'SMALLINT(6) DEFAULT 0',
			'created'=>'datetime NOT NULL',
			'updated'=>'datetime DEFAULT NULL',
		));
		
		$this->createIndex('name', 'person', 'name');
		$this->createIndex('shortname', 'person', 'shortname');
		$this->createIndex('email', 'person', 'email');
		
		$this->insert('person', array(
			'name'=>'Eva',
			'email'=>'eva@mail.com',
			'type'=>1,
			'created'=>new CDbExpression('NOW()'),
		));
		
		$this->insert('person', array(
			'name'=>'Nina',
			'email'=>'nina@mail.com',
			'type'=>1,
			'created'=>new CDbExpression('NOW()'),
		));
		
		$this->insert('person', array(
			'name'=>'Flemming',
			'email'=>'flemming@mail.com',
			'type'=>1,
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('person');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}