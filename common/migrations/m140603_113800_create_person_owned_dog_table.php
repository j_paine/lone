<?php

class m140603_113800_create_person_owned_dog_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('person_owned_dog', array(
			'person_id'=>'integer NOT NULL',
			'dog_id'=>'integer NOT NULL',
			'from'=>'date DEFAULT NULL',
			'to'=>'date DEFAULT NULL',
		));
		
		$this->addPrimaryKey('pk_person_owned_dog', 'person_owned_dog', array('person_id','dog_id'));
		
		$this->addForeignKey(
			'fk_person_owned_dog_person', 
			'person_owned_dog', 'person_id', 
			'person', 'id', 
			'CASCADE', 'NO ACTION'
		);
		
		$this->addForeignKey(
			'fk_person_owned_dog_dog', 
			'person_owned_dog', 'dog_id', 
			'dog', 'id', 
			'CASCADE', 'NO ACTION'
		);
		
		$this->insert('person_owned_dog', array(
			'person_id'=>2,
			'dog_id'=>1,
			'from'=>'2001-03-15',
		));
		
		$this->insert('person_owned_dog', array(
			'person_id'=>2,
			'dog_id'=>2,
			'from'=>'2012-01-03'
		));
	}

	public function down()
	{
		$this->dropTable('person_owned_dog');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}