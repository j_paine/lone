<?php

class m140603_113736_create_photo_has_person_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('photo_has_person', array(
			'person_id'=>'integer NOT NULL',
			'photo_id'=>'integer NOT NULL',
		));
		
		$this->addPrimaryKey('pk_photo_has_person', 'photo_has_person', array('person_id','photo_id'));
		
		$this->addForeignKey(
			'fk_photo_has_person_person', 
			'photo_has_person', 'person_id', 
			'person', 'id', 
			'CASCADE', 'NO ACTION'
		);
		$this->addForeignKey(
			'fk_photo_has_person_photo', 
			'photo_has_person', 'photo_id', 
			'photo', 'id', 
			'CASCADE', 'NO ACTION'
		);
	}

	public function down()
	{
		$this->dropTable('photo_has_person');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}