<?php

class m140603_113723_create_post_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('post', array(
			'id'=>'pk',
			'title'=>'string NOT NULL',
			'contents'=>'text NOT NULL',
			'created'=>'datetime NOT NULL',
			'updated'=>'datetime DEFAULT NULL',
			'user_id'=>'integer NULL',
		));
		
		$this->createIndex('title', 'post', 'title');
		$this->createIndex('user', 'post', 'user_id');
		
		$this->addForeignKey(
			'fk_post_user', 
			'post', 'user_id', 
			'user', 'id', 
			'SET NULL', 'NO ACTION'
		);
		
		$this->insert('post', array(
			'title'=>'Test Post',
			'contents'=>'testing...',
			'created'=>new CDbExpression('NOW()'),
			'user_id'=>1,
		));
	}

	public function down()
	{
		$this->dropTable('post');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}