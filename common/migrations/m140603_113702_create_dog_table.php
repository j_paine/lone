<?php

class m140603_113702_create_dog_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('dog', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
			'comment'=>'text DEFAULT NULL',
			'gender'=>'boolean DEFAULT NULL',
			'dob'=>'date DEFAULT NULL',
			'dod'=>'date DEFAULT NULL',
			'mum_id'=>'integer DEFAULT NULL',
			'dad_id'=>'integer DEFAULT NULL',
			'stmb'=>'string DEFAULT NULL',
			'created'=>'datetime NOT NULL',
			'updated'=>'datetime DEFAULT NULL',
		));
		
		$this->createIndex('name', 'dog', 'name');
		$this->createIndex('stmb', 'dog', 'stmb');
		
		$this->addForeignKey(
			'fk_dog_mum', 
            'dog', 'mum_id', 
            'dog', 'id', 
            'SET NULL', 'NO ACTION'
		);
		$this->addForeignKey(
			'fk_dog_dad', 
            'dog', 'dad_id', 
            'dog', 'id', 
            'SET NULL', 'NO ACTION'
		);
		
		$this->insert('dog', array(
			'name'=>'Una',
			'comment'=>'Værsgo Una, spise!',
			'dob'=>'2001-03-15',
			'created'=>new CDbExpression('NOW()'),
		));
		
		$this->insert('dog', array(
			'name'=>'Xavi',
			'comment'=>'Who\'s a good boy?',
			'dob'=>'2012-01-03',
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('dog');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}