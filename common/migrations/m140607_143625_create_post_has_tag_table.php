<?php

class m140607_143625_create_post_has_tag_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('post_has_tag', array(
			'post_id'=>'integer NOT NULL',
			'tag_id'=>'integer NOT NULL',
		));
		
		$this->addPrimaryKey('pk_post_has_tag', 'post_has_tag', array('post_id','tag_id'));
		
		$this->addForeignKey(
			'fk_post_has_tag_post', 
			'post_has_tag', 'post_id', 
			'post', 'id', 
			'CASCADE', 'NO ACTION'
		);
		$this->addForeignKey(
			'fk_post_has_tag_tag', 
			'post_has_tag', 'tag_id', 
			'tag', 'id', 
			'CASCADE', 'NO ACTION'
		);
		$this->insert('post_has_tag', array(
			'post_id'=>1,
			'tag_id'=>1,
		));
		$this->insert('post_has_tag', array(
			'post_id'=>1,
			'tag_id'=>2,
		));
	}

	public function down()
	{
		$this->dropTable('post_has_tag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}