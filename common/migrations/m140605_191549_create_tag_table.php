<?php

class m140605_191549_create_tag_table extends CDbMigration
{
	public function up()
	{	
		$this->createTable('tag', array(
			'id'=>'pk',
			'name'=>'string NOT NULL',
		));
		
		$this->insert('tag', array(
			'name'=>'dogs',
		));
		
		$this->insert('tag', array(
			'name'=>'knitting',
		));
		
		$this->insert('tag', array(
			'name'=>'motorcycles',
		));
		
	}

	public function down()
	{
		$this->dropTable('tag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}