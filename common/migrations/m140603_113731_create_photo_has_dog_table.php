<?php

class m140603_113731_create_photo_has_dog_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('photo_has_dog', array(
			'dog_id'=>'integer NOT NULL',
			'photo_id'=>'integer NOT NULL',
		));
		
		$this->addPrimaryKey('pk_photo_has_dog', 'photo_has_dog', array('dog_id','photo_id'));
		
		$this->addForeignKey(
			'fk_photo_has_dog_dog', 
			'photo_has_dog', 'dog_id', 
			'dog', 'id', 
			'CASCADE', 'NO ACTION'
		);
		$this->addForeignKey(
			'fk_photo_has_dog_photo', 
			'photo_has_dog', 'photo_id', 
			'photo', 'id', 
			'CASCADE', 'NO ACTION'
		);
	}

	public function down()
	{
		$this->dropTable('photo_has_dog');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}