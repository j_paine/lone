<?php

class m140603_113751_create_person_bred_dog_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('person_bred_dog', array(
			'person_id'=>'integer NOT NULL',
			'dog_id'=>'integer NOT NULL',
		));
		
		$this->addPrimaryKey('pk_person_bred_dog', 'person_bred_dog', array('person_id','dog_id'));
		
		$this->addForeignKey(
			'fk_person_bred_dog_person', 
			'person_bred_dog', 'person_id', 
			'person', 'id', 
			'CASCADE', 'NO ACTION'
		);
		
		$this->addForeignKey(
			'fk_person_bred_dog_dog', 
			'person_bred_dog', 'dog_id', 
			'dog', 'id', 
			'CASCADE', 'NO ACTION'
		);
		
		$this->insert('person_bred_dog', array(
			'person_id'=>2,
			'dog_id'=>2,
		));
	}

	public function down()
	{
		$this->dropTable('person_bred_dog');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}