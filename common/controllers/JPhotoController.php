<?php

class JPhotoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $path='/../images/';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','img','thumb', 'gallery'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','upload'),
				'expression'=>'Yii::app()->user->status==="1"',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'expression'=>'Yii::app()->user->role==="1"',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionGallery()
	{
		$dataProvider=new CActiveDataProvider('Photo');
		$this->render('common.views.photo.gallery',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Display image with id $id
	 * @param integer $id of the image to display
	 */
	public function actionImg($id)
	{
		$file=$this->loadImage($id);
		$type=getimagesize($file);
		$image=readfile($file);
		
		header("Content-type: {$type['mime']}");
		echo $image;
	}
	
	public function actionThumb($id)
	{
		$file=$this->loadImage($id,true);
		$type=getimagesize($file);
		$image=readfile($file);
		
		header("Content-type: {$type['mime']}");
		echo $image;
	}
	
	public function actionUpload()
	{
		$path=realpath(Yii::app()->basePath.$this->path);
		$publicPath=Yii::app()->request->baseUrl."/photo/img/";

		header('Vary: Accept');
		if(isset($_SERVER['HTTP_ACCEPT']) 
			&& (strpos($_SERVER['HTTP_ACCEPT'], 'application/json')!==false)) 
		{
			header('Content-type: application/json');
		} else {
			header('Content-type: text/plain');
		}

		//Here we check if we are deleting an uploaded file
		if(isset($_GET["_method"]) && $_GET["_method"]=="delete") 
		{
			if($_GET["file"][0]!=='.') 
			{
				$file = $path.$_GET["file"];
				if(is_file($file)) 
				{
					unlink($file);
				}
			}
			echo json_encode(true);
		} 
		else if(isset($_POST['Photo']))
		{
			$model=new Photo;
			$model->attributes=$_POST['Photo'];
			$model->filename=CUploadedFile::getInstance($model, 'filename');
			
			if($model->filename!==null) {
				
				$filename=$path.'/'.$model->filename;
				if($model->save()) 
				{
					$model->filename->saveAs($filename);
					$image=new EasyImage($filename);
					$image->resize(null,200);
					$image->save($path.'/thumbs/'.$model->filename);
					echo json_encode(array(
						array(
							"name" => $model->filename,
							"type" => $model->filename->type,
							"size" => $model->filename->size,
							"url" => $publicPath.$model->id,
							"thumbnail_url" => $publicPath.$model->id,
							"delete_url" => $this->createUrl("upload", array(
								"_method" => "delete",
								"file" => $model->filename
							)),
							"delete_type" => "POST"
						)
					));	
				} else {
					echo json_encode(array( 
						array("error" => $model->getErrors('filename')),
					));
					//Yii::log( "XUploadAction: ".CVarDumper::dumpAsString( $model->getErrors( ) ),
					//	CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction" 
					//);
				}
			} else {
				throw new CHttpException( 500, "Could not upload file" );
			}
		}
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('common.views.photo.view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Photo('create');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Photo'])) 
		{
			$model->attributes=$_POST['Photo'];
			if($model->validate())
				$model->save();
		}

		$this->render('common.views.photo.create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Photo'])) {
			$model->attributes=$_POST['Photo'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('common.views.photo.update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Photo');
		$this->render('common.views.photo.index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Photo('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Photo'])) {
			$model->attributes=$_GET['Photo'];
		}

		$this->render('common.views.photo.admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Photo the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Photo::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}
	
	public function loadImage($id,$thumb=null)
	{
		$model=$this->loadModel($id);
		$path=Yii::app()->basePath.$this->path;
		if($thumb!==null)
		{
			$path.='thumbs/';
		}
		$file=$path.$model->filename;
		return $file;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Photo $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='photo-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}