<?php

class JUserBehavior extends CActiveRecordBehavior
{
	public $userAttribute = 'user_id';
	
	public function beforeSave($event)
	{
		if ($this->getOwner()->getIsNewRecord() && ($this->userAttribute !== null)) {
			$this->getOwner()->{$this->userAttribute} = Yii::app()->user->id;
		}
	}
}