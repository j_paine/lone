<?php

/**
 * This is the model class for table "dog".
 *
 * The followings are the available columns in table 'dog':
 * @property integer $id
 * @property string $name
 * @property string $comment
 * @property boolean $gender
 * @property string $dob
 * @property string $dod
 * @property integer $mum_id
 * @property integer $dad_id
 * @property string $stmb
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Dog $dad
 * @property Dog $mum
 * @property Comment[] $comments
 * @property Person[] $bred_by
 * @property Person[] $owned_by
 * @property Photo[] $photos
 */
class Dog extends CActiveRecord
{
	
	public $children=array();
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dog';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>255),
			array('dob, dod', 'date', 'allowEmpty'=>true, 'format'=>'dd-mm-yy'),
			array('comment, name, stmb', 'filter', 'filter'=>'strip_tags'),
			array('gender', 'boolean'),
			array('mum_id, dad_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, comment, gender, dob, dod, mum_id, dad_id, stmb, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dad' => array(self::BELONGS_TO, 'Dog', 'dad_id'),
			'mum' => array(self::BELONGS_TO, 'Dog', 'mum_id'),
			'comments' => array(self::MANY_MANY, 'Comment', 'dog_has_comment(dog_id, comment_id)'),
			'bred_by' => array(self::MANY_MANY, 'Person', 'person_bred_dog(dog_id, person_id)'),
			'owned_by' => array(self::MANY_MANY, 'Person', 'person_owned_dog(dog_id, person_id)'),
			'photos' => array(self::MANY_MANY, 'Photo', 'photo_has_dog(dog_id, photo_id)'),
		);
	}
	
	public function behaviors()
	{
		return array(
			'CTimestampBehavior'=>array(
				'class'=>'zii.behaviors.CTimestampBehavior',
				'createAttribute'=>'created',
				'updateAttribute'=>'updated',
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'comment' => 'Comment',
			'gender' => 'Sex',
			'dob' => 'Born',
			'dod' => 'Died',
			'mum_id' => 'Mother',
			'dad_id' => 'Father',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('dod',$this->dod,true);
		$criteria->compare('mum_id',$this->mum_id);
		$criteria->compare('dad_id',$this->dad_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider('Dog', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
